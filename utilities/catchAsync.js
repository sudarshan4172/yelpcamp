//this wraps the Async function in try and catch

const { findOneAndReplace } = require("../models/campground")

module.exports= func=>{
  return (req,res,next) => {
    func(req,res,next).catch(next); //this catches the error and pass it to next
  }
}