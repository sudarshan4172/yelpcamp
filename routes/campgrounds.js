const express = require('express');
const router = express.Router({ mergeParams: true });
const campgrounds = require('../controllers/campgrounds');
const catchAsync = require('../utilities/catchAsync');
const { validateCampground, isLoggedIn, isAuthor } = require('../middleware');
const multer = require('multer'); //this is used to parse the body of the multipart form
const { storage } = require('../cloudinary/index');
const upload = multer({ storage });


//middlewares



//ROUTES

//all campgrounds show page
router.get('/', catchAsync(campgrounds.index));

//new campground
router.get('/new', isLoggedIn, campgrounds.renderNewForm)

//new campground post req
router.post('/', isLoggedIn, upload.array('image'), validateCampground, catchAsync(campgrounds.createCampground))

//show page of particular campground
router.get('/:id', catchAsync(campgrounds.showCampground))

//edit from for a campground
router.get('/:id/edit', isLoggedIn, isAuthor, catchAsync(campgrounds.renderEditForm));

//req to edit a campground
router.put('/:id', isLoggedIn, isAuthor, upload.array('image'), validateCampground, catchAsync(campgrounds.updateCampground))

//delete a campgroud
router.delete('/:id', isLoggedIn, isAuthor, catchAsync(campgrounds.deleteCampground))

module.exports = router;