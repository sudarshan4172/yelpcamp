const User = require('../models/user');


module.exports.renderRegisterForm = (req, res) => {
  res.render('users/register', { title: 'User Registration' });
}

module.exports.register = async (req, res, next) => {
  try {
    const { username, email, password } = req.body;
    const user = new User({ username, email });
    const registeredUser = await User.register(user, password);
    req.login(registeredUser, err => {
      if (err) return next(err);
      req.flash('success', 'Welcome to YelpCamp!');
      res.redirect('/campgrounds');
    })
  }
  catch (e) {
    req.flash('error', e.message);
    res.redirect('/register');
  }
}

module.exports.renderLoginForm = (req, res) => {
  res.render('users/login', { title: 'Login' });
}

module.exports.login = async (req, res) => {
  req.flash('success', 'Welcome back!');
  const redirectUrl = req.session.returnTo || '/campgrounds';
  // if(redirectUrl){
  //   return res.redirect(redirectUrl);
  // }
  delete req.session.returnTo; //this is to remove the stored url from session once we are done with it
  res.redirect(redirectUrl);
}

module.exports.logout = (req, res) => {
  req.logout();
  req.flash('success', 'Logged out successfully!')
  res.redirect('/');
}