// this file contains the logic of the routes
const Campground = require('../models/campground');
const {cloudinary} = require('../cloudinary/index');
const mbxGeocoding = require('@mapbox/mapbox-sdk/services/geocoding');
const mapBoxToken = process.env.mapbox_token;
const geocoder = mbxGeocoding({accessToken: mapBoxToken});


module.exports.index = async (req, res) => {
  const campgrounds = await Campground.find({});
  res.render('campgrounds/index', { campgrounds, title: 'Campgrounds' });
}

module.exports.renderNewForm = (req, res) => {
  res.render('campgrounds/new', { title: 'New' });
}

module.exports.createCampground = async (req, res) => {

  const geoData = await geocoder.forwardGeocode({
    query: req.body.campground.location,
    limit: 1
  }).send()
  const campground = new Campground(req.body.campground);
  campground.geometry = geoData.body.features[0].geometry;
  campground.images = req.files.map(f => ({ url: f.path, filename: f.filename }));
  campground.author = req.user._id,
    await campground.save();
  req.flash('success', 'Campgorund added!');
  res.redirect(`/campgrounds/${campground._id}`);
}

module.exports.showCampground = async (req, res) => {
  const campground = await Campground.findById(req.params.id).populate({ path: 'reviews', populate: { path: 'author' } }).populate('author');
  if (!campground) {
    req.flash('error', 'Campground not found!');
    return res.redirect('/campgrounds');
  }
  res.render('campgrounds/show', { campground, title: `${campground.title}` });
}

module.exports.renderEditForm = async (req, res) => {
  const campground = await Campground.findById(req.params.id);
  if (!campground) {
    req.flash('error', 'Campground not found!');
    return res.redirect('/campgrounds');
  }
  res.render('campgrounds/edit', { campground, title: `${campground.title}` });
}

module.exports.updateCampground = async (req, res) => {
  const { id } = req.params;
  // console.log(req.body)
  const campground = await Campground.findByIdAndUpdate(id, { ...req.body.campground });
  const imgs = req.files.map(f => ({ url: f.path, filename: f.filename }));
  campground.images.push(...imgs);
  await campground.save();
  //updating images
  if (req.body.deleteImages) {
    //this is to delete images from cloudinary
    for(let filename of req.body.deleteImages){
      await cloudinary.uploader.destroy(filename);
    }
    //this is to delete images from mongo
    await campground.updateOne({ $pull: { images: { filename: { $in: req.body.deleteImages } } } })
  }

  req.flash('success', 'Campground Updated!')
  res.redirect(`/campgrounds/${campground._id}`);
  // res.send(req.body.campground);
}

module.exports.deleteCampground = async (req, res) => {
  const { id } = req.params;
  await Campground.findByIdAndDelete(id);
  req.flash('success', 'Campground Deleted!')
  res.redirect('/campgrounds');
}