if(process.env.NODE_ENV !== "production"){
  require('dotenv').config();
}

const port = process.env.PORT || 3000;
const express = require('express');
const path = require('path');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const ExpressError = require('./utilities/ExpressError');
const ejsMate = require('ejs-mate');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const falsh = require('connect-flash');
const passport = require('passport');
const localStrategy = require('passport-local');
const User = require('./models/user');
const helmet = require('helmet');

const mongoSanitize = require('express-mongo-sanitize');

const dbUrl = process.env.db_url;
// const dbUrl = 'mongodb://localhost:27017/yelp-camp';


//requiring the routes
const campgroundRoutes=require('./routes/campgrounds');
const reviewRoutes=require('./routes/reviews');
const userRoutes = require('./routes/users');


//DB connection
mongoose.connect(dbUrl, {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", ()=> {
  console.log("Database Connected...");
});


const app = express();


app.set('view engine', 'ejs'); //this set the view files type
app.set('views', path.join(__dirname, 'views')); //this sets the views directory as default

app.engine('ejs', ejsMate); //this is used for layout

app.use(express.static(path.join(__dirname, 'public'))); //this serves static files
app.use(express.urlencoded({extended: true})); //this parse the body
app.use(methodOverride('_method')); //this is used to override methods PUT and DELETE
app.use(mongoSanitize());

const secret = process.env.secret || 'thisshouldbeabettersecret';
const store = new MongoDBStore({
  uri: dbUrl,
  collection: 'sessions',
  secret,
  touchAfter: 24*60*60 //in seconds
})

store.on('error', function(e){
  console.log("Session store error ", e);
})
const sessionConfig = {
  store,
  name: 'session',
  secret,
  resave: false,
  saveUninitialized: true,
  cookie: {
    httpOnly: true,
    // secure: true,
    expires: Date.now() + 1000*60*60*24*7, //this are the msec in a week
    maxAge: 1000*60*60*24*7
  }
}
app.use(session(sessionConfig));
app.use(falsh());


const scriptSrcUrls = [
  "https://api.mapbox.com/",
  "https://code.jquery.com/",
  "https://cdn.jsdelivr.net/",
]

const styleUrls = [
  "https://api.mapbox.com/",
  "https://api.tiles.mapbox.com/",
  "https://fonts.gstatic.com/",
  "https://fonts.googleapis.com/",
  "https://use.fontawesome.com/"
]

const connectUrls = [
  "https://api.mapbox.com/",
  "https://a.tiles.mapbox.com/",
  "https://b.tiles.mapbox.com/",
  "https://events.mapbox.com/",
]

const fontSrc = [
  "https://fonts.gstatic.com/",
]


app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: [],
      connectSrc: ["'self'", ...connectUrls],
      scriptSrc: ["'unsafe-inline'", "'self'", ...scriptSrcUrls],
      styleSrc: ["'self'", "'unsafe-inline'", ...styleUrls],
      workerSrc: ["'self'", "blob:"],
      objectSrc: [],
      imgSrc: [
        "'self'",
        "blob:",
        "data:",
        "https://res.cloudinary.com/sudarshan2109/",

      ],
      fontSrc: ["'self'", ...fontSrc],
    },
  })
);

app.use(passport.initialize());
app.use(passport.session()); //make sure that session is used before passport.session()
passport.use(new localStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser()); //how to store and remove it in the session
passport.deserializeUser(User.deserializeUser());


//------middleware------

app.use((req,res,next)=>{
  // console.log(req.query)
  res.locals.currentUser = req.user; //this gives the user if user is logged in else returns undefined
  res.locals.success = req.flash('success');
  res.locals.error = req.flash('error');
  next();
})


//-----ROUTES-----

app.get('/', (req, res)=> {
  res.render('home');
})

app.use('/campgrounds', campgroundRoutes);

app.use('/campgrounds/:id/reviews', reviewRoutes);

app.use('/', userRoutes);








//404 error handler
app.all('*', (req,res,next)=>{
  // res.send("Page Not Found....404");
  next(new ExpressError('Page Not Found', 404));
})

//error handler middleware
app.use((err,req,res,next)=>{
  const {statusCode=500} = err;
  if(!err.message) err.message='Something went wrong!'
  res.status(statusCode).render('error',{err, title:'Error'});
})




app.listen(port, ()=> {
  console.log(`YelpCamp server started at port ${port}`);
})
// this is remote
