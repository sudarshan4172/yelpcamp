module.exports.descriptors=[
  'Forest',
  'Ancient',
  'Petrified',
  'Roaring',
  'Cascade',
  'Tumbling',
  'Silent',
  'Redwood',
  'Bullfrog',
  'Maple',
  'Misty',
  'Ocean'
]

module.exports.places=[
  'Flats',
  'Village',
  'Canyon',
  'Pond',
  'Group Camp',
  'Horse Camp',
  'Camp',
  'River',
  'Creek',
  'Sands'
]