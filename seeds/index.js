const mongoose = require('mongoose');
const cities = require('./cities');
const { places, descriptors } = require('./seedHelpers');
const Campground = require('../models/campground');  
const url = process.env.db_url
mongoose.connect('mongodb+srv://admin:Sudu1221@cluster0.dve5w.mongodb.net/YelpCamp?retryWrites=true&w=majority', {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection error:"));
db.once("open", () => {
  console.log("Database Connected...");
});

const sample = array => array[Math.floor(Math.random() * array.length)];

const seedDB = async () => {
  await Campground.deleteMany({});
  for (let i = 0; i < 300; i++) {
    const random1000 = Math.floor(Math.random() * 1000);
    const price = Math.floor(Math.random() * 990) + 10;
    const camp = new Campground({
      author: '60b6392b9f0c0688d5213fed',
      location: `${cities[random1000].city}, ${cities[random1000].state}`,
      title: `${sample(descriptors)} ${sample(places)}`,
      price: price,
      description: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quae, consequuntur? Excepturi optio, a quos reiciendis fugiat dicta pariatur velit illo.',
      images: [
        {
          url: "https://res.cloudinary.com/sudarshan2109/image/upload/v1622560418/YelpCamp/tent-1490599_1920_r9oe2a.jpg",
          filename: "tent-1490599_1920_r9oe2a"
        },
        // {
        //   url: "https://res.cloudinary.com/sudarshan2109/image/upload/v1621209595/YelpCamp/qcvfhjoz7vvzr4ylcao6.jpg",
        //   filename: "YelpCamp/qcvfhjoz7vvzr4ylcao6"
        // }
      ]
    })
    camp.geometry = {
      type:"Point",
      // coordinates:[75.56667,21.01667]
      coordinates:[cities[random1000].longitude, cities[random1000].latitude]
    }
    await camp.save();
  }
}
seedDB().then(() => {
  mongoose.connection.close();
});